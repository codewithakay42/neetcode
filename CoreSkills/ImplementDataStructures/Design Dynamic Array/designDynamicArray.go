package main

type Array interface {
	Get(i int) int
	Set(i int, n int)
	PushBack(n int)
	PopBack() int
	GetSize() int
	GetCapacity() int
}

type DynamicArray struct {
	capacity int
	size     int
	array    []int
}

func NewDynamicArray(capacity int) *DynamicArray {
	return &DynamicArray{
		capacity: capacity,
		size:     0,
		array:    make([]int, capacity),
	}
}

func (d *DynamicArray) Get(i int) int {
	return d.array[i]
}

func (d *DynamicArray) Set(i int, n int) {
	d.array[i] = n
}

func (d *DynamicArray) PushBack(n int) {
	if d.size == d.capacity {
		d.Resize()
	}
	d.array[d.size] = n
	d.size++
}

func (d *DynamicArray) Resize() {
	newCapacity := d.capacity * 2
	newSlice := make([]int, newCapacity)
	copy(newSlice, d.array)
	d.array = newSlice
	d.capacity = newCapacity
}

func (d *DynamicArray) GetSize() int {
	return d.size
}

func (d *DynamicArray) GetCapacity() int {
	return d.capacity
}
