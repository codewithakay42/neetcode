class DynamicArray:

    def __init__(self, capacity: int):
        """
        Initializing the capacity of the Dynamic Array. Where capacity > 0
        """
        self.capacity = capacity
        self.size = 0
        self.array = [None] * capacity

    def get(self, i: int) -> int:
        """
        return element at index i assuming that i is valid
        """
        return self.array[i]

    def set(self, i: int, n: int) -> None:
        """
        Set element at index i to n; assuming i is valid
        """
        self.array[i] = n

    def pushback(self, n: int) -> None:
        """
        push element n to the end of the array
        """
        if self.size == self.capacity:
            self.resize()
        self.array[self.size] = n
        self.size += 1

    def popback(self) -> int:
        """
        will pop and return element at the the end of the array. Assuming the array is not empty.
        """
        self.size -= 1
        return self.array[self.size]

    def resize(self) -> None:
        new_capacity = self.capacity * 2
        new_array = [None] * new_capacity

        for i in range(self.size):
            new_array[i] = self.array[i]
        self.array = new_array
        self.capacity = new_capacity

    def getSize(self) -> int:
        return self.size

    def get_capacity(self) -> int:
        return self.capacity
